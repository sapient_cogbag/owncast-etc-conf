# owncast-etc-conf
Utility script and systemd service template for starting owncast with config specified in a standard toml file (rather than the hybrid CLI/web config interface).

Contributions for other init systems are welcome!

